\documentclass[letterpaper,serif]{rpg-module}
\usepackage{hyperref}

\begin{document}

\title{Terror in Abrandil}
\author{Charlie Shaeffer}
\subtitle{An adventure for three to four characters of 5th level}
\abstract{
The isolated town of Abrandil has been cursed by a demon. At each week's end for the past three weeks, a portion of
the town has disappeared from the lot it stood on -- only to return in the morning, burned into a husk. The locals are
terrified, but nobody seems to be leaving town. Can the heroes put an end to this demonic phenomenon?
}

% \maketitle

\twocolumn
\showtitle

\part{Introduction}
The \emph{Terror in Abrandil} is a short adventure that can be completed in 2 to 3 sessions.
It's set in a remote town named Abrandil and is not tied to any particular region of the Forgotten Realms,
allowing it to be included in an existing campaign with relative ease.

\section{Background}
Some time ago, a \textbf{horned devil} named Ka'Thazel was banished from Avernus, the first layer of the Nine Hells, by the
Archduchess Zariel for suspected cowardice. Undeterred, Ka'Thazel began devising a scheme to regain Zariel's favor and prove his loyalty.

Through a mysterious dealer of cursed items, Ka'Thazel came into possession of two artifacts which he could use to end
his banishment: an \textbf{Orb of Planar Shifting} and an \textbf{Idol of Hellish Contagion}. The dealer pointed Ka'Thazel
in the direction of the nearby town of Abrandil. A small town of 100, Abrandil lies on the outskirts of the city of Felrune.
With a steady supply of traders coming to and from the big city, the people of Abrandil have made a living largely through
farmsteads and providing goods to travelers.

Ka'Thazel placed a curse around Abrandil's perimeter using the Idol of Hellish Contagion. Those cursed have been
prevented from leaving the town as they await their fate. The Orb of Planar Shifting is bound to his home of Avernus;
upon use it sends the wielder and the surrounding area to Avernus for 4 hours. With each use, Ka'Thazel became more
attuned to orb, allowing him to increase the size of the area he was able to send to Avernus.

Shortly after the first attack, an ambitious \textbf{cambion} named Vosk discovered Ka'Thazel's goals and saw
it as an opportunity to rise the demonic ranks. He shifted from Avernus to Abrandil in the second week along with a
couple \textbf{shadow demons} and several \textbf{imps}. Vosk charmed the local acolyte Elbar and has been using him
to undermine the church's investigation into Ka'Thazel's attacks.

Meanwhile, Malimor, a Priest of Pelor, received a mysterious letter instructing him to search the church's library for
clues regarding the attacks. He pored over many scrolls and books in the church's collection until he stumbled across a hidden library
with ancient texts. These texts contained symbols matching the ones left where the destroyed houses stand. Further
investigation lead him to a forgotten altar to an unknown god beneath the chapel. Through his imperfect translation of
the ancient texts, Malimor believes that he can perform a ritual to exchange his life for the life of the demon
terrorizing the town.

\section{Overview}
The adventure is split up into three parts, one for each day until the final attack.

In part \ref{eighth-day}, the characters enter
town and learn about the horrific phenomenon that's been wreaking havoc on the townsfolk. They discover that parts of
the town have been disappearing only to return in the morning, burnt beyond recognition.

In part \ref{ninth-day}, the characters begin their investigation with the assistance of a few members from the local church. At
nightfall, the demon tormenting the town introduces himself as Ka'Thazel through an imp vessel.

In part \ref{tenth-day}, the characters complete their investigation and make final preparations before the attack.

\part{The Eighth Day}
\label{eighth-day}
The characters enter town on the eighth day, two days before the next expected attack.

\begin{boxtext}
As you come to a clearing in the woods, you find yourself at the footsteps of a small town -- a village, really -- just in time for dinner.
With a weak smile, a young lady points you towards the local inn, the Rumsworth, and hurries away. Passersby tense up as they see you.
As you open the door to the Rumsworth Inn, several patrons look up at you with worried eyes before carrying on their private conversations.
\end{boxtext}

\section{The Rumsworth}
The Rumsworth Inn is owned by Sigran Rumsworth. A man named Serulious is currently pouring mead at the bar. The barmaid Elsea
is tending to the patrons at the tables.

Any patron at the inn will provide the characters with the following information, should they ask for it:
\begin{itemize}
    \item At each week's end for the past three weeks, a portion of the town disappeared from the lot it stood on
    \item During those nights, portals opened up and demons flooded through, terrifying and killing townsfolk
    \item At dawn, the demons vanished and everything that had disappeared returned to where it once was, although it
    had been burned down to a husk
    \item Any person inside the area when it disappeared was found flayed and burned to death
    \item Each following attack resulted in more destruction and demons than the one before it
    \item Some people attempted to leave town, but upon crossing the town's boundary their flesh fell from their bones
    until they bled to death
    \item The head priest Yorem has been leading the investigation with help from the other clergymen
    \item There are only two more nights until the next attack, and everybody is terrified
\end{itemize}

A couple of the rooms upstairs have already been taken, but two more double rooms remain if the characters ask for
lodging. If the characters are looking for more options, Serulious lets them know the church in town always welcomes
guests with open arms.

\newpage
\part{The Ninth Day}
\label{ninth-day}
It's the day before the next expected attack. The characters are given the opportunity to investigate.
If the characters don't go out to investigate or interview townspeople, they are invited to
\nameref{the-church} and are greeted by the head priest Yorem.

\section*{Developments}
\begin{description}
    \item [8 pm]
    Yorem withdraws to the Rumsworth to drink. See the \nameref{yorem} section for more character information.
    If the characters are at the Rumsworth for dinner, Serulious asks them to help him home. After getting inside,
    he promptly falls asleep on the living room floor. This leads to the next development.

    \item [10 pm]
    At nightfall, Ka'Thazel appears in the town square through an Imp vessel. He uses the time to
    introduce himself to the townsfolk he's tormented for so long and tells of the glory that tomorrow will bring and the
    destruction that will occur.

    After this monologue, he summons various lesser fiends (\textbf{manes}, \textbf{imps}, \textbf{quasits}).
    They spring up and attack random townspeople.

    A large group of fiends attacks the church directly; One \textbf{hezrou} and three \textbf{quazits}. The characters
    are alerted of this attack and are asked to help.

    \item [2 am]
    If not already dealt with, Vosk and his minions attack people in their homes in the middle of the night.

    If he's about to be defeated and all of his allies are dead, Vosk will attempt to plane shift home so that he can
    return on the tenth night and fight with Ka'Thazel.

\end{description}

\section{The Church}
\label{the-church}
The church of Abrandil is an all-faith church, although the priests choose to follow Pelor, the god of Sun.
There are obscured, ancient symbols in the church worshipping Torog, the evil god of the Underdark.
Hidden areas were added to the church allowing people to worship him in secret. It's unknown who introduced these areas.

\subsection{Library}
\label{library}
The library holds many texts related to several well-known gods, but most are written about Pelor.
\begin{boxtext}
The library walls are lined with shelves, nearly full of books and parchments. A small window above the shelves on
the back wall lets in a smattering of light. The room floor is littered with books, many of which are open. A small
path leads to a table in the back corner with an extinguished lantern.
\end{boxtext}

Malimor spends all of his free time here, poring over books. If he's not already out with the characters, he's
most likely in this room researching the pact he's intent on making. If the characters interrupt him in the library and do not
intend on leaving him alone, he storms out and leaves the library empty for the characters to investigate. For more
information about Malimor, see the \nameref{malimor} section.

Characters rummaging through the shelves can make a \textbf{DC 15 Perception} check which reveals a hidden alcove with
ancient texts. The area is opened by pulling on a particular book which is tied to the latch.

A \textbf{DC 20 Arcana} check allows a character to decipher those texts. The character is able to detect a pattern of
recurring symbols. The symbols are for the god Torog. Characters familiar with the evil gods or the underdark may
recognize the symbols.

\subsection{Chapel}
The chapel appears to be pretty standard.

\begin{boxtext}
Three rows of pews are split with an aisle in the center, from the main doors to the altar. Light streams through
the four glass windows, two on each side.
\end{boxtext}

A door at the back of the chapel on the left leads to the library in area \ref{library}.
The other door leads to Yorem's Office in area \ref{yorem-office}

Characters who spend time looking around the chapel may make a \textbf{DC 15 Religion} check to notice that the
symbols inside do not match symbols for the more common gods.

A secret underground altar to Torog (area \ref{hidden-altar}) is revealed to characters who can decipher a pattern in the symbols.
A \textbf{DC 15 Investigation} check reveals the pattern. If a character deciphered the ancient texts from the
hidden library alcove, they can identify the pattern with a \textbf{DC 10 Arcana} check.

\subsection{Hidden Altar}
\label{hidden-altar}
Torog's altar is hidden at the base of a stone staircase.

\begin{boxtext}
Cold stone steps spiral into darkness.
\end{boxtext}

After descending halfway down the stairs, a character must make a \textbf{DC 15 Wisdom} saving throw. On a success, the
character is able to continue down the stairs. Otherwise, the character is unable to pick up the courage to go any
further.

Read the following to characters who make it to Torog's altar:
\begin{boxtext}
As you descend further, you find a small room with a stone altar in the center. The air is cold and sharp.
\end{boxtext}

A button which opens a hidden panel can be found with a \textbf{DC 15 Perception} check if the character is actively
investigating the room.

Behind the hidden panel lies a coat rack with
\begin{itemize}
    \item A Cloak of Magical Resistance
    \item A Ring of Spell Store
    \item A Gem of Truesight
\end{itemize}

\subsection{Yorem's Office}
\label{yorem-office}

The office has nothing of note. Just a desk, chair, and some scrolls.
A locked door leads to the vestry in area \ref{vestry}.

\subsection{Vestry}
\label{vestry}

\begin{boxtext}
The door opens up to a plain room with an armoire and wooden shelves. On the shelves are several ceremonial items for
common religious rites. Three ornate items are carefully presented on the center shelf: A long wooden box crafted out
of dark wood, a small box lined with silk, and a strapless knapsack made out of fine linen.
\end{boxtext}

The vestry holds a few church heirlooms which Yorem plans to entrust to the party to assist with defeating the demons.
\begin{itemize}
    \item Sword of Radiance, in the long wooden box
        \newline The sword is sheathed in leather and held in place with a silken pillow. It's identical to Flame Tongue
        in the DMG except with radiant light and damage rather than fire.
    \item Ring of Evasion, in the small silk-lined box
        \newline A golden ring with small hummingbird crafted from fine jewels.
    \item Boots of Elvenkind, in the strapless knapsack
        \newline Cedar shoe-trees are placed in the boots to hold their shape.
\end{itemize}

\section{The Priests}

\subsection{Yorem}
\label{yorem}
Yorem was born and raised in Abrandil, and since then he's become the town's head priest.
He tends to keep to himself although he has recently become more reclusive as the pressure to save the townsfolk from
the demons has grown. Like many in town, he's turned to alcohol to dampen his feelings and drown out the thoughts
of helplessness from his mind.

\subsection{Ozen}
Ozen moved to Abrandil from the nearby town of Felrune. He's worried that despite their best efforts, they won't be able
to stop the demon. In fights, he'll help however possible. Use the \textbf{Priest} stat block for his abilities.
He and Elsea the barmaid are secretly in love, and he is ashamed of admitting this to Yorem.

\subsection{Malimor}
\label{malimor}
Malimor was born and raised in Abrandil. He's a devout and rigorous student of the cloth and will go to great lengths
to protect the town -- even if it means trading his own life. He will assist the characters in any way he can, however
he keeps his plans to sacrifice himself through a ritual secret. On the ninth day, a \textbf{DC 20 Insight} check is
required to discover he's hiding something. On the tenth day, it is reduced to \textbf{DC 15}.

\subsection{Elbar}
Elbar was born and raised in Abrandil. For over a week, he has been charmed by a \textbf{cambion} named Vosk who
currently resides in his home's basement. A \textbf{DC 15 Insight} check is required for characters asking him questions
related to his recent activities.
Characters who cast \emph{Detect Magic} will be able to see a faint magical aura surrounding Elbar.

\section{Elbar's Home}
Elbar's home is in a neighborhood east of the town center.

\subsection{The Basement}
Flickering light emanates from the bottom of the stairs.

Vosk, a \textbf{cambion}, two \textbf{shadow demons}, and four \textbf{imps} are hiding in the basement.
Vosk has charmed Elbar and is using him to undermine the investigation in hopes that his actions will allow him to rise
the demonic ranks.

If he's about to be defeated and all of his allies are dead, Vosk attempts to plane shift home to Avernus in order to
return on the tenth night.

If Vosk is killed, his charm on Elbar stops immediately.

\section{The Attacked Sites}
The previously attacked sites are in pretty bad shape. Having been taken to Avernus, most of the wooden supports and
furniture caught fire and burned completely. The stone foundations are mostly intact, but still damaged.

Making a \textbf{DC 20/15/10 Investigation} check on the first, second, or third site reveals etchings and sigils
underneath the rubble and burned under each house. The difficulty is increased for the first sites because time
has passed to allow the planar shifting magic to fade and the sites to have been picked clean by raiders and
and animals. Characters attempting to decipher the sigils can make a \textbf{DC 15 Arcana} check to notice that
they relate to the Nine Hells.

A \textbf{DC 15 Perception} check notes that each attack enveloped a larger area. Characters taking a closer look
can make a \textbf{DC 20 Intelligence} check to deduce that the next attack may be able to affect the whole town.

\newpage

\part{The Tenth Day}
\label{tenth-day}
Tonight's the night. One of two scenarios will play out at nightfall;
either \nameref{torogs-pact} or \nameref{kathazels-attack}.

Until the attack, the characters have some time to continue investigating areas and people.

\section*{Developments}
\begin{description}
    \item[10 am]
        The townsfolk gather outside the church and begin rioting.
    \item[7 pm]
        Yorem entrusts the characters with the church heirlooms stored in the \nameref{vestry} to assist
        in defeating the demons.
    \item[8 pm]
        If the characters have not been able to discover and stop Malimor's plan, he retreats into the
        underground altar to begin the bargain ritual with Torog, the God of the Underdark.
\end{description}

\section{The Townsfolk Riots}
If the townsfolk are not satisfied with the investigation's progress, they may begin to riot in the morning.
This is an opportunity to add some obstacles for the characters to overcome. This section can be skipped or mitigated if
the party has made significant progress towards identifying or solving the problem. In addition, the party may attempt
to appeal to the townsfolk to regain their favor.

\subsection*{Example Riot Consequences}
\begin{itemize}
    \item The businesses are shut down, and the people are not willing to cooperate with the characters.
    \item The townsfolk tie up the church members to use them as a sacrifice to appease Ka'Thazel.
        With Yorem detained, he cannot give the party the church heirlooms.
        Malimor may attempt to escape in order to complete his pact in attempt to save the town.
\end{itemize}

\section{Torog's Pact}
\label{torogs-pact}
If Malimor is uninterrupted during his ritual in the underground altar, his pact with Torog completes.
If he's otherwise prevented from completing the ritual, skip to \nameref{kathazels-attack}.

On completion of the ritual, a beam of purple light shines from the underground altar up into the sky
before disappearing. Characters who follow the light to its source find Malimor weeping at the underground altar
as his life slowly drains. He apologizes for what he's done, believing that he could bargain to end the town's suffering
and asks the characters not to speak of his involvement to others.

A purple beam strikes down into the town square once Malimor dies. A \textbf{behir} and two \textbf{mezzoloths}
emerge from the beam, summoned on behalf of Torog. Ka'Thazel follows shortly after, having been forcefully shifted into
Abrandil by Torog. The two \textbf{mezzoloths} cast hold person on Ka'Thazel while the \textbf{behir} fulfills Malimor's pact
by devouring Ka'Thazel.

To satisfy its own end of the bargain, the \textbf{behir} proceeds to rampage through the town until it's destroyed.

\section{Ka'Thazel's Attack}
\label{kathazels-attack}
At nightfall, Ka'Thazel appears in the town square and begins his attack alongside two \textbf{hell hounds} and several
lesser demons; \textbf{quazits}, \textbf{manes}, etc. If Vosk has not been defeated yet, he also returns and fights
alongside Ka'Thazel.

\subsection{The Orb of Planar Shifting}

The \textbf{Orb of Planar Shifting} is placed upon a dais in the town square and it draws power from four pillar
conduits which surround the orb in the shape of a square. The ritual takes time, drawing energy from Avernus until it
can create a portal large enough to cover the town. The conduits help contain this power and aid in speeding
up the casting time.

Unperturbed, the ritual completes after 3 rounds of combat. If completed, the town is teleported to Avernus for
one hour (see \nameref{teleported-to-avernus}). It takes two successful attacks to destroy the orb. The orb starts at
\textbf{AC 20}. Every attack lowers the AC by 2.

Each conduit starts at \textbf{AC 20} and only one successful attack is required to destroy it. Every attack lowers the
AC by 5. For each destroyed conduit, the ritual takes an additional turn to complete.


\subsection{Teleported to Avernus}
\label{teleported-to-avernus}

Abrandil is transported to Avernus in a state of limbo for an hour. If the orb is destroyed within the hour, the spell
is cut short and the town returns to the Material Plane.

Avernus' extreme heat sweeps through the area and deals \emph{1d6 fire damage} per round to every character.
In addition, a \textbf{barlgura} and two \textbf{spined devils} enter the area, terrorizing townspeople.

\section{Destroying The Idol}
Destroying the orb and defeating Ka'Thazel is not sufficient for lifting the curse which prevents townsfolk from
leaving the town alive. Characters who loot Ka'Thazel's body will find the \textbf{Idol of Hellish Contagion}.
The idol has \textbf{AC 10}. Destroying the idol lifts the curse.


\newpage

\tableofcontents

\begin{productidentity}

\item This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
    To view a copy of this license, visit \href{http://creativecommons.org/licenses/by-sa/4.0/}{http://creativecommons.org/licenses/by-sa/4.0/}
    or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

\item \modulecopyright

\end{productidentity}

\end{document}
