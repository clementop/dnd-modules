# Dungeons and Dragons 5e Modules

## Precompiled PDFs
PDFs are compiled through GitLab's Continuous Integration feature:
- [browse files](https://gitlab.com/clementop/dnd-modules/-/jobs/artifacts/master/browse?job=build)
- [download zip](https://gitlab.com/clementop/dnd-modules/-/jobs/artifacts/master/download?job=build)

## Dependencies

### LaTeX
Each module is written in LaTeX. A Makefile is provided to compile each of the documents in
`src/` to PDFs in `out` using the `pdflatex` command.

### rpg-module
A number of modules are written using the rpg-module document class which can be found
[on slithy's github page](https://github.com/slithy/rpg_module/).

## Compiling the PDFs
The modules can be compiled using LaTeX via the `make` command on unix-based machines.

```
$ make
...

$ tree
.
├── Makefile
├── out
│   └── Terror-in-Abrandil.pdf
├── README.md
├── src
│   └── Terror-in-Abrandil.tex
└── tmp
    ├── Terror-in-Abrandil.aux
    ├── Terror-in-Abrandil.log
    ├── Terror-in-Abrandil.out
    └── Terror-in-Abrandil.toc

3 directories, 8 files
```

The generated files can be cleaned up as well.

```
$ make clean
rm -f -r tmp
rm -f -r out

$ tree
.
├── Makefile
├── README.md
└── src
    └── Terror-in-Abrandil.tex

1 directory, 3 files
```
