OUT_DIR = out
TMP_DIR = tmp

FLAGS = -halt-on-error -output-directory $(TMP_DIR)

all: install

%.pdf: %.tex %.toc
	mkdir -p $(TMP_DIR)
	pdflatex $(FLAGS) $<

%.toc: %.tex
	mkdir -p $(TMP_DIR)
	pdflatex $(FLAGS) $<

install: src/*.pdf
	mkdir -p $(OUT_DIR)
	mv $(TMP_DIR)/*.pdf $(OUT_DIR)

ci: src/*.pdf
	mv $(TMP_DIR)/*.pdf .

clean:
	$(RM) -r $(TMP_DIR)
	$(RM) -r $(OUT_DIR)
	$(RM) *.pdf
